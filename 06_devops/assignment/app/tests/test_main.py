"""Main unit test file"""

from fastapi.testclient import TestClient
from app.main import app, courses_db

client = TestClient(app)


def test_read_course():
    """Test successful return of a course data"""
    course_id = "CS-E4190"
    res = client.get(f"/courses/{course_id}")

    assert res.status_code == 200
    assert res.json() == courses_db["CS-E4190"]


def test_read_nonexistent_course():
    """Test return of a nonexistent course"""
    res = client.get("/courses/unknown-id")

    assert res.status_code == 404
    assert res.json() == {"detail": "Course does not exist"}


def test_create_course():
    """Test addition of a new course"""
    payload = {
        "id": "CS-G4242",
        "name": "Hichhikers's guide to Cloud Software",
        "instructor": "Ford Prefect",
        "keyword": ["cloud", "hitchhiking", "whales"]
    }

    res = client.post('/courses/', json=payload)

    assert res.status_code == 200
    assert res.json() == payload

def test_create_existing_course():
    """Test addition of a new course but the course ID already exists"""
    res = client.post('/courses/', json=courses_db["CS-E4190"])

    assert res.json() == {"detail": "Course already exists"}
    assert res.status_code == 400
