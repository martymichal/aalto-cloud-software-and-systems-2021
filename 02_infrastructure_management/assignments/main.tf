variable "vm_name_input" {
    default = "vm-name"
}

output "vm_name" {
    value = google_compute_instance.node.name
}

output "public_ip" {
    value = google_compute_instance.node.network_interface[0].access_config[0].nat_ip
}

terraform {
    required_providers {
        google = {
            source = "hashicorp/google"
        }
    }
}

provider "google" {
    project = "cs-e4190-327413"
    region  = "europe-north1"
    zone    = "europe-north1-a"
}

resource "google_compute_instance" "node" {
    name         = var.vm_name_input
    machine_type = "f1-micro"

    boot_disk {
        initialize_params {
            image = "debian-cloud/debian-10"
        }
    }

    network_interface {
        network = "default"

        access_config {}
    }
}
