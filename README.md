# Aalto Cloud Software and Systems 2021

- Passed with 3/5
- Period 2; winter semester
- A very well organized course providing a comprehensive introduction into cloud software by covering the most notable aspects of the field

# Quick Summary

Every week a new topic was introduced in the form of a textual lecture (coupled with a synthetically generated podcast) coupled with slides with more technical information and tutorials providing a practical introduction into the introduced technologies. Every week a series of tasks had to be done to gather a sufficient amount of points. These tasks were either naswering theoretical questions about the technologies or accomplishing practical exercises.
