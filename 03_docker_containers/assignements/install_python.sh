#!/bin/sh

python_deps="build-essential libssl-dev zlib1g-dev libncurses5-dev libncursesw5-dev libreadline-dev libsqlite3-dev libgdbm-dev libdb5.3-dev libbz2-dev libexpat1-dev liblzma-dev libffi-dev uuid-dev"
dir_name="Python-3.9.0"
tool_deps="curl apt-transport-https ca-certificates"

if [ $# = 0 ]; then
	echo "Specify version of Python to install (3.8 or 3.9)"
	exit 1
fi

if ! [ "$1" = "3.8" ] && ! [ "$1" = "3.9" ]; then
	echo "Incorrect version of Python. Supported versions: 3.8, 3.9"
	exit 2
fi

apt-get update

if [ "$1" = "3.8" ]; then
	apt-get install -y --no-install-recommends python3.8
	exit 0
fi

# Python 3.9 dependencies
apt-get install -y --no-install-recommends $python_deps
apt-get install -y --no-install-recommends $tool_deps

# Python 3.9 sources
curl  https://www.python.org/ftp/python/3.9.0/Python-3.9.0.tgz | tar -axz
cd "./$dir_name"

# Build & Install
./configure
make -j
make -j install

# Clean build
cd ..
rm -r "./$dir_name"

apt-get remove -y $python_deps
apt-get remove -y $tool_deps
