"""The Python implementation of the gRPC route guide client."""

from __future__ import print_function

import logging
import random

import grpc
from proto import restaurant_pb2
from proto import restaurant_pb2_grpc


def restaurant_food_order(stub):
	msg = restaurant_pb2.RestaurantRequest(orderID="1", items=[ "chips" ])
	print(stub.FoodOrder(msg))
	msg = restaurant_pb2.RestaurantRequest(orderID="2", items=[ "pizza", "soup" ])
	print(stub.FoodOrder(msg))
def restaurant_drink_order(stub):
	pass

def restaurant_dessert_order(stub):
	pass


def run():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = restaurant_pb2_grpc.RestaurantStub(channel)
        print("-------------- FoodOrder  ----------------")
        restaurant_food_order(stub)
        print("-------------- DrinkOrder ----------------")
        restaurant_drink_order(stub)
        print("-------------- DessertOrder --------------")
        restaurant_dessert_order(stub)


if __name__ == '__main__':
    logging.basicConfig()
    run()
