import grpc
import sys
from concurrent import futures
from proto import restaurant_pb2
from proto import restaurant_pb2_grpc

RESTAURANT_ITEMS_FOOD = ["chips", "fish", "burger", "pizza", "pasta", "salad"]
RESTAURANT_ITEMS_DRINK = ["water", "fizzy drink", "juice", "smoothie", "coffee", "beer"]
RESTAURANT_ITEMS_DESSERT = ["ice cream", "chocolate cake", "cheese cake", "brownie", "pancakes", "waffles"]


class Restaurant(restaurant_pb2_grpc.RestaurantServicer):
    def FoodOrder(self, request, context):
        status = 'ACCEPTED'
        for item in request.items:
            if item not in RESTAURANT_ITEMS_FOOD:
                status = 'REJECTED'
                break
                
        return restaurant_pb2.RestaurantResponse(orderID=request.orderID, status=status)

    def DrinkOrder(self, request, context):
        status = 'ACCEPTED'
        for item in request.items:
            if item not in RESTAURANT_ITEMS_DRINK:
                status = 'REJECTED'
                break
                
        return restaurant_pb2.RestaurantResponse(orderID=request.orderID, status=status)
    
    def DessertOrder(self, request, context):
        status = 'ACCEPTED'
        for item in request.items:
            if item not in RESTAURANT_ITEMS_DESSERT:
                status = 'REJECTED'
                break
                
        return restaurant_pb2.RestaurantResponse(orderID=request.orderID, status=status)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    restaurant_pb2_grpc.add_RestaurantServicer_to_server(Restaurant(), server)
    
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    serve()
