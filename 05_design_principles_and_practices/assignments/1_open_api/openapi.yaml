openapi: 3.0.1
info:
  title: API design with OpenAPI
  description: 'Project 1 in chapter 5 of the Cloud Software and Systems course'
  contact:
    email: ondrej.michal@aalto.fi
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  version: 1.0.0
servers:
- url: https://localhost/api
- url: http://localhost/api
paths:
  /photo:
    post:
      summary: Creates a photo
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Photo'
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '422':
          description: Unprocessable entity
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /photo/{photo_id}:
    get:
      summary: Gets a photo by ID
      parameters:
        - in: path
          name: photo_id
          required: true
          schema:
            type: 'string'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Photo'
        '404':
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

    put:
      summary: Updates a photo by ID
      parameters:
        - in: path
          name: photo_id
          required: true
          schema:
            type: 'string'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Photo'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '404':
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '422':
          description: Unprocessable entity
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

    delete:
      summary: Deletes a photo by ID
      parameters:
        - in: path
          name: photo_id
          required: true
          schema:
            type: 'string'
      responses:
        '204':
          description: No Content
        '404':
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /photos:
    get:
      summary: Gets all photos
      parameters:
        - in: 'query'
          name: 'access'
          schema:
            type: "string"
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Photos'
        '404':
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

components:
  schemas:
    Photo:
      type: "object"
      properties:
        id:
          type: "string"
          readOnly: true
        name:
          type: "string"
          maxLength: 20
        description:
          type: "string"
          maxLength: 100
        access:
          type: "string"
          pattern: '^public$|^private$'
        location:
          type: "string"
        file:
          type: "string"
        created_date:
          type: "string"
          format: "date-time"
        modified_date:
          type: "string"
          format: "date-time"
      required:
        - name
        - access
        - location
        - file
        - created_date
        - modified_date
    
    Photos:
      type: "array"
      items:
        $ref: '#/components/schemas/Photo'

    Success:
      type: "object"
      properties:
        message:
          type: "string"
        id:
          type: "string"

    Error:
      type: "object"
      properties:
        message:
          type: "string"
