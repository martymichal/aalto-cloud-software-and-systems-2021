from db import Album
from flask import request, jsonify, Blueprint, Response

listAlbum = Blueprint("listAlbum", __name__)


@listAlbum.route("/listAlbum", methods=["POST"])
def post():
    album = Album(**request.get_json()).save()
    return {"message": "Album successfully created", "id": str(album.id)}, 201


@listAlbum.route("/listAlbum/<album_id>", methods=["GET"])
def get_album_id(album_id: str):
    album = Album.objects.get_or_404(id=album_id)
    return {"id": album_id, "name": album.name}, 200


@listAlbum.route("/listAlbum/<album_id>", methods=["PUT"])
def put(album_id: str):
    body = request.get_json()

    album = Album.objects.get_or_404(id=album_id).update(**body)
    return {"message": "Album successfully updated", "id": album_id}, 200


@listAlbum.route("/listAlbum/<album_id>", methods=["DELETE"])
def delete(album_id: str):
    album = Album.objects.get_or_404(id=album_id)
    album.delete()
    return {"message": "Album successfully deleted", "id": album_id}, 200
