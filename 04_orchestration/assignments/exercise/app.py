import db
from flask import Flask
from flask_mongoengine import MongoEngine

from photos import listPhoto
from albums import listAlbum

app = Flask(__name__)

app.config["MONGODB_SETTINGS"] = {
    "db": "flask-database",
    "host": "localhost",
    "port": 27017,
}

app.register_blueprint(listPhoto)
app.register_blueprint(listAlbum)

mongodb = db.initialize_db(app)

if __name__ == "__main__":
    app.run()
