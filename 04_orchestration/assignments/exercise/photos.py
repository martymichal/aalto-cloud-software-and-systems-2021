from bson.objectid import ObjectId
from db import Album, Photo
from flask import request, jsonify, Blueprint, Response
import base64
import codecs
import json
import sys
import urllib
import uuid

listPhoto = Blueprint('listPhoto', __name__)

@listPhoto.route('/listPhoto', methods=['POST'])
def post():
    album = Album.objects(name="Default").first()
    if album == None:
        album = Album(name="Default").save()

    photo = Photo(**request.form, image_file=request.files["file"], albums=[album.id]).save()

    return { "message": 'Photo successfully created', "id": str(photo.id) }, 201

@listPhoto.route('/listPhoto', methods=['GET'])
def get():
    tag = request.args.get('tag')
    name = request.args.get('albumName')

    photos = Photo.objects()
    result = []
    for photo in photos:
        if tag != None and tag not in photo.tags:
            continue

        if name != None:
            found = False
            for albums in photo.albums:
                if name == album.name:
                    found = True
                    break

            if not found:
                continue

        base64_data = codecs.encode(photo.image_file.read(), 'base64')
        image = base64_data.decode('utf-8')
        if photo.location == None:
            location = ""
        else:
            location = photo.location
        result.append({ 'name': photo.name, 'location': location, 'file': image })

    return jsonify(result), 200

@listPhoto.route('/listPhoto/<photo_id>', methods=['GET', 'PUT', 'DELETE'])
def get_photo_id(photo_id: str):
    photo = Photo.objects.get_or_404(id=photo_id)

    if request.method == "GET":
        base64_data = codecs.encode(photo.image_file.read(), 'base64')
        image = base64_data.decode('utf-8')

        albums = []
        dbref = photo.albums
        print(dbref, file=sys.stderr)
          #  albums.append(str(album))

        return {
            'name': photo.name,
            'tags': photo.tags,
            'location': photo.location,
            'albums': albums,
            'file': image }, 200
    elif request.method == "PUT":
        body = request.get_json()
        keys = body.keys()
        if body and keys:
            if "albums" in keys:
                body["albums"] = [ObjectId(element) for element in body["albums"]]
        photo.update(**body)
        return { "message": 'Photo successfully updated', "id": photo_id }, 200
    elif request.method == "DELETE":
        photo.delete()
        return { "message": 'Photo successfully deleted', "id": photo_id }, 200

    return 500

