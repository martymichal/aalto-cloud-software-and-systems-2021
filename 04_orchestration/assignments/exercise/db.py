from flask import Flask
from flask_mongoengine import MongoEngine

db = MongoEngine()

class Photo(db.Document):
    name = db.StringField(required=True)
    tags = db.ListField()
    location = db.StringField()
    image_file = db.ImageField(required=True)
    albums = db.ReferenceField('Album')

class Album(db.Document):
    name = db.StringField(required=True, unique=True)
    description = db.StringField()

def initialize_db(app: Flask):
    db.init_app(app)
